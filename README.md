**REST API Server using Django**

This is a django based backend server source code which has two modules.

1. Module 1 -Authentication module
2. Module 2 -Product module

---

## Setup Guidelines

1. Clone the repository.
2. Run make, this will create virtual enviroinment.
3. Run make install, this will install the pip packages.
4. Activate you venv to enter into you venv and start working - "source bin/activate"

## Usage

I have actually hosted this backend REST API server here(65.2.3.115), so all the routes are live at http://65.2.3.115:8000.

Here are some of the screenshots of how it works.

![picture](https://i.ibb.co/Hr9Rd1Q/loign.png)

![picture](https://i.ibb.co/QHk8Jf7/create-SKU.png)

![picture](https://i.ibb.co/Dp02kD0/get-details.png)

![picture](https://i.ibb.co/fF4m4v0/update-details.png)

![picture](https://i.ibb.co/HzRLP8C/delete-product.png)

---