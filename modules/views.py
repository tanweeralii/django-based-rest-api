from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer
from modules.models import users, products
from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from passlib.hash import pbkdf2_sha256
from rest_framework_jwt.utils import jwt_payload_handler
import jwt
from rest_framework_simplejwt.backends import TokenBackend

class login(APIView):
    def get(self, request): 
        res = users.objects.filter(username=request.GET['userName']).exists()
        if res is False:
            content = {'message': 'User Does not Exist!'}
            return Response(content)
        else:
            encrypted = users.objects.get(username=request.GET['userName']).password
            verify = pbkdf2_sha256.verify(request.GET['password'], encrypted)
            if verify is True:
                details = users.objects.get(username=request.GET['userName'], password=encrypted)
                payload = jwt_payload_handler(details)
                token = jwt.encode(payload, settings.SECRET_KEY)
                content = {'message': 'Login Successfull', 'token' : token}
                return Response(content)
            else:
               content = {'message': 'Wrong Password'}
               return Response(content)

class signup(APIView):

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        dob = request.data.get('DOB')
        phoneNumber = request.data.get('phoneNumber')
        res = users.objects.filter(username=username).exists()
        if res is True:
            content = {'message': 'User Already Exists'}
            return Response(content)
        else:
            encrypted = pbkdf2_sha256.encrypt(password, rounds=12000, salt_size=32)
            print(encrypted)
            result = users(username=username, DOB=dob, password=encrypted, phoneNumber=phoneNumber);
            result.save();
            content = {'message': 'User Successfully Added'}
            return Response(content)

class addProducts(APIView):

    def post(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '' ).strip('"')
        validation = validating(token)
        if validation is True:
            res = products.objects.filter(sku_name=request.data.get('sku_name')).exists()
            if res is True:
                content = {'message': 'Product Already Exists'}
                return Response(content)
            else:
                result = products(sku_name=request.data.get('sku_name'), sku_category=request.data.get('sku_category'), price=request.data.get('price'))
                result.save()
                content = {'message': 'Product Added Successfully'}
                return Response(content)
        else:
            content = {'message' : 'Invalid Token! Authorization Failed'}
            return Response(content)

class getProdDetails(APIView):

    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '' ).strip('"')
        validation = validating(token)
        if validation is True:
            print(request.GET['id'])
            if_id_exists = products.objects.filter(id=request.GET['id']).exists()
            if if_id_exists is True:
                res = products.objects.get(id=request.GET['id'])
                content = {'sku_name' : res.sku_name, 'sku_category' : res.sku_category, 'price' : res.price}
                return Response(content)
            else:
                content = {'message' : 'Invalid ID'}
                return Response(content)
        else:
            content = {'message' : 'Invalid Token'}
            return Response(content)

    def put(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '' ).strip('"')
        validation = validating(token)
        if validation is True:
            if_id_exists = products.objects.filter(id=request.GET['id']).exists()
            if if_id_exists is True:
                res = products.objects.get(id=request.GET['id'])
                if_already_exists = products.objects.filter(sku_name=request.data.get('sku_name')).exists()
                if if_already_exists is True:
                    content = {'message': 'Given product already exists'}
                    return Response(content)
                else:
                    res.sku_name = request.data.get('sku_name')
                    res.sku_category = request.data.get('sku_category')
                    res.price = request.data.get('price')
                    res.save()
                    content = {'message': 'Updated Successfully'}
                    return Response(content)
            else:
                content = {'message' : 'Id is not valid'}
                return Response(content)
        else:
            content = {'message' : 'Invalid Token'}
            return Response(content)

    def delete(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '' ).strip('"')
        validation = validating(token)
        if validation is True:
            res = products.objects.filter(id=request.GET['id']).exists()
            if res is False:
                content = {'message': 'Invalid ID'}
                return Response(content)
            else:
                products.objects.filter(id=request.GET['id']).delete()
                content = {'message' : 'Deleted Succesfully'}
                return Response(content)
        else:
            content = {'message' : 'Invalid Token'}
            return Response(content)

def validating(token):
    data = {'token': token}
    try:
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)
        user = valid_data['username']
        check_user = users.objects.filter(username=user).exists()
        if check_user is True:
            return True
        else:
            return False
    except:
        return False
