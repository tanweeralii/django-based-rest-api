from django.db import models

class users(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=13, default='test')
    phoneNumber = models.CharField(max_length=13)
    password = models.TextField()
    DOB = models.DateField()

class products(models.Model):
   id = models.AutoField(primary_key=True)
   sku_name = models.CharField(max_length=30)
   sku_category = models.CharField(max_length=30)
   price = models.IntegerField()
